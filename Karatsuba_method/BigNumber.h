#include <iostream>
#include <cstring>

using namespace std;

typedef unsigned char BASE;
typedef unsigned short int D_BASE;
const int BASE_SIZE = sizeof(BASE)*8;
const D_BASE b = 1 << BASE_SIZE;

class BigNumber {
    BASE *digits;
    int len, maxLen;

    void normalize();

    int BN_Compare(BigNumber & bn1, BigNumber & bn2);
public:
    BigNumber(int lenMax = 1, bool random_numbers = false);

    BigNumber(BigNumber & bn);

    ~BigNumber();

    friend ostream& operator << (ostream & r, BigNumber & v) {
        for(int i = v.len-1; i >= 0; i--) {
            BASE x = v.digits[i];
            bool skipped_leading_zeros = false;
            for(int j = BASE_SIZE/4 - 1; j >= 0; j--) {
                BASE c = (x >> j*4) & 15;
                if(i == v.len-1 && !skipped_leading_zeros) {
                    while(c == 0 && j > 0) {
                        j--;
                        c = (x >> j*4) & 15;
                    }
                    skipped_leading_zeros = true;
                }
                if(c >= 0 && c <= 9) {
                    r << char(c + '0');
                }else {
                    r << char(c + 'a' - 10);
                }
            }
        }
        return r;
    }

    friend istream& operator >> (istream & r, BigNumber & v) {
        delete [] v.digits;
        char str[1024];
        r >> str;

        int str_len = strlen(str), msd;
        for(msd = 0; msd < str_len && str[msd] == '0'; msd++);
        if(msd == str_len) msd--;

        v.maxLen = v.len = (str_len-msd)*4/BASE_SIZE + (((str_len-msd) * 4)% BASE_SIZE);
        v.digits = new BASE[v.maxLen];
        for(int i = 0; i < v.maxLen; i++) {
            v.digits[i] = 0;
        }

        for(int i = str_len-1, j = 0, k = 0; i >= msd; i--) {
            char c = str[i];
            BASE x = 0;
            if (c >= '0' && c <= '9') {
                x = c - '0';
            } else if (c >= 'a' && c <= 'f') {
                x = c - 'a' + 10;
            } else {
                throw ("Content error!");
            }
            x <<= k;
            v.digits[j] |= x;

            k = (k + 4) % BASE_SIZE;
            if(k == 0) {
                j++;
            }
        }
        return r;
    }

    BigNumber operator = (BigNumber const & bn);

    bool operator > (BigNumber & bn);

    bool operator < (BigNumber & bn);

    bool operator >= (BigNumber & bn);

    bool operator <= (BigNumber & bn);

    bool operator == (BigNumber & bn);

    bool operator != (BigNumber & bn);

    BigNumber operator + (BigNumber & bn);

    BigNumber operator + (BASE & num);

    BigNumber operator += (BigNumber & bn);

    BigNumber operator - (BigNumber & bn);

    BigNumber operator -= (BigNumber & bn);

    BigNumber operator * (BASE num);

    BigNumber operator * (BigNumber & bn);

    BigNumber operator / (BASE num);

    BASE operator % (BASE num);

    BigNumber operator / (BigNumber bn);

    BigNumber operator % (BigNumber bn);

    BigNumber base_10_input(char *string);

    void base_10_output(char* string);

    int get_len();
    int get_maxLen();

    BASE operator [] (int i);

    friend BigNumber karatsuba(BigNumber u, BigNumber v);
};