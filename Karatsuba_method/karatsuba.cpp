#include "BigNumber.h"

BigNumber karatsuba(BigNumber u, BigNumber v) {
    int n;
    if(u.len > v.len) {
        n = u.len/2 + u.len%2;
    } else {
        n = v.len/2 + v.len%2;
    }

    BigNumber u0(n), u1(n), v0(n), v1(n);
    for(int i = 0; i < n; i++) {
        if(i < u.len) {
            u0.digits[i] = u.digits[i];
        }
        if(n+i < u.len) {
            u1.digits[i] = u.digits[n + i];
        }
        if(i < v.len) {
            v0.digits[i] = v.digits[i];
        }
        if(n+i < v.len) {
            v1.digits[i] = v.digits[n + i];
        }
    }
    u0.len = v0.len = n;
    u1.len = v1.len = n;
    u1.normalize();
    v1.normalize();

    BigNumber A, B, C, tmp, tmp2, result;
    A = u1 * v1;
    B = u0 * v0;
    C = A + B;
    tmp = u1*v0;
    C = C + tmp;
    tmp = u0*v1;
    C = C + tmp;
    tmp2 = BigNumber(A.len+2*n);
    for(int i = 0; i < A.len; i++) {
        tmp2.digits[2*n + i] = A.digits[i];
    }
    tmp2.len = tmp2.maxLen;
    result = B;
    result = result + tmp2;
    tmp2 = C - A;
    tmp2 = tmp2 - B;
    tmp = BigNumber(n+1);
    tmp.digits[n] = 1;
    tmp.len = n+1;
    tmp2 = tmp2 * tmp;
    result = result + tmp2;
    result.normalize();
    return result;
}