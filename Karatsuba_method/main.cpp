#include "BigNumber.h"
#include <iostream>
#include <fstream>

int main() {
    try {
        ifstream input("input.txt");
        ofstream output("output.txt");
        if(!(input.is_open() && output.is_open())) {
            throw("File does not exist!");
        }
        while(!input.eof()) {
            BigNumber u, v, z;
            char us[100], vs[100], zs[200];
            bool flag = true;
            char c;

            int i = 0;
            do {
                c = input.get();
                if ((c < '0' || c > '9') && c != ' ') {
                    flag = false;
                    while(c != '\n' && !input.eof()) {
                        c = input.get();
                    }
                } else if(c != ' ') {
                    us[i++] = c;
                }
            } while (flag && c != ' ');
            if(!flag) continue;
            us[i] = '\0';

            i = 0;
            do {
                c = input.get();
                if ((c < '0' || c > '9') && c != '\n') {
                    flag = false;
                    while(c != '\n' && !input.eof()) {
                        c = input.get();
                    }
                } else if(c != '\n') {
                    vs[i++] = c;
                }
            } while (flag && c != '\n');
            if(!flag) continue;
            vs[i] = '\0';

            u.base_10_input(us);
            v.base_10_input(vs);

            BigNumber zero;
            z = karatsuba(u, v);
            z.base_10_output(zs);
            output << zs << (char)(0xa);
        }
        input.close();
        output.close();
    } catch(const char *msg) {
        cout << "Error: " << msg << '\n';
        return 0;
    } catch(...) {
        cout << "Unknown error!\n";
        return 0;
    }
    return 1;
}