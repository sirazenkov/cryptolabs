#include "BigNumber.h"
#include <iostream>
#include <fstream>
#include <regex>

using namespace std;

int main() {
    try {
        ifstream input("input.txt");
        ofstream output("output.txt");
        if(!(input.is_open() && output.is_open())) {
            throw("File does not exist!");
        }
        string line;
        while(getline(input, line)) {
            int space_counter = 0;
            for(int i = 0; i < line.size(); i++) {
                if(line[i] == ' ') {
                    space_counter++;
                }
            }
            if(space_counter != 3) {
                continue;
            }

            BigNumber x, y, m, bb, z;
            char xs[100], ys[100], ms[100], bs[100], zs[100];
            stringstream ss;
            ss.str(line);
            ss >> xs;
            if(!regex_match(xs, regex("^([1-9][0-9]*|0)$"))) {
                continue;
            }

            ss >> ys;
            if(!regex_match(ys, regex("^([1-9][0-9]*)$"))) {
                continue;
            }

            ss >> ms;
            if(!regex_match(ms, regex("^([2-9][0-9]*|[1-9][0-9]+)$"))) {
                continue;
            }

            ss >> bs;
            if(!regex_match(bs, regex("^([2-9][0-9]*|[1-9][0-9]+)$"))) {
                continue;
            }

            x.base_10_input(xs);
            y.base_10_input(ys);
            m.base_10_input(ms);
            bb.base_10_input(bs);

            if(bb.hamming_weight() > 1) {
                continue;
            }
            z = exponentiation_according_to_Montgomery(x, y, m, bb);
            BigNumber minus_one; BASE one = 1; minus_one = minus_one + one; minus_one.inverse();
            if(z == minus_one) {
                continue;
            }
            z.base_10_output(zs);
            output << zs << (char) (0xa);
        }
        input.close();
        output.close();
    } catch(const char *msg) {
        cout << "Error: " << msg << '\n';
        return 0;
    } catch(...) {
        cout << "Unknown error!\n";
        return 0;
    }
    return 1;
}