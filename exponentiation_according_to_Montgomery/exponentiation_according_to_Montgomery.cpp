#include "BigNumber.h"

BigNumber exponentiation_according_to_Montgomery(BigNumber &x, BigNumber &y, BigNumber &m, BigNumber &bb) {
    int k = m.number_length(bb), n = y.msb();
    BigNumber m_, R2, m0, d; m0 = m % bb;
    m0.non_negative = false;
    while(!m0.non_negative) {
        m0 = m0 + bb;
    }

    BigNumber one; BASE oneb = 1; one = one + oneb;

    d = EEA(bb, m0, &m_);
    while(!m_.non_negative) {
        m_ = m_ + bb;
    }

    if(d != one || x >= m) {
        one.non_negative = false;
        return one; // error code -1
    }

    BigNumber dk;
    BASE dki = 2 * k;
    dk = dk + dki;
    R2 = from_left_to_right(bb, dk, m);
    BigNumber bk = bb;
    for(int i = 1; i < k; i++) {
        bk = bk * bb;
    }
    BigNumber x_; x_ = multiplication_according_to_Montgomery(x, R2, m, bb);
    BigNumber z = x_;
    for(int i = n-2; i >= 0; i--) {
        z = multiplication_according_to_Montgomery(z, z, m, bb);
        if(y[i] == 1) {
            z = multiplication_according_to_Montgomery(z, x_, m, bb);
        }
    }
    BigNumber res; res = transformation_according_to_Montgomery(z, bk, m, bb);
    return res;
}

BigNumber multiplication_according_to_Montgomery(BigNumber &x, BigNumber &y, BigNumber &m, BigNumber &bb) {
    int k = m.number_length(bb);
    BigNumber one; BASE oneb = 1; one = one + oneb;
    BigNumber m_, m0, d; m0 = m % bb;
    m0.non_negative = false;
    while(!m0.non_negative) {
        m0 += bb;
    }
    d = EEA(bb, m0, &m_);
    while(!m_.non_negative) {
        m_ = m_ + bb;
    }

    if(d != one || x >= m || y >= m) {
        one.non_negative = false;
        return one; // error code -1
    }

    BigNumber z, tmp, tmp2, tmp3;
    for(int i = 0; i < k; i++) {
        tmp2 = x.get_digit(bb, i);
        tmp3 = y.get_digit(bb, 0);
        tmp = tmp3 * tmp2;
        tmp2 = z.get_digit(bb, 0);
        BigNumber u; u = tmp2 + tmp;
        u = u * m_;
        u = u % bb;
        tmp2 = x.get_digit(bb, i);
        tmp = y * tmp2;
        z = z + tmp;
        tmp = u * m;
        z = z + tmp;
        z = z / bb;
    }

    if(z > m) {
        z = z - m;
    }
    return z;
}

BigNumber transformation_according_to_Montgomery(BigNumber &x, BigNumber &R, BigNumber &m, BigNumber &bb) {
    int k = m.number_length(bb);
    BigNumber one; BASE oneb = 1; one = one + oneb;
    BigNumber m_, m0, tmp, d; m0 = m % bb; tmp = m*R;
    m0.non_negative = false;
    while(!m0.non_negative) {
        m0 += bb;
    }
    d = EEA(bb, m0, &m_);
    while(!m_.non_negative) {
        m_ = m_ + bb;
    }

    if(d != one || x >= tmp) {
        one.non_negative = false;
        return one; // error code -1
    }

    BigNumber z; z = x;
    for(int i = 0; i < k; i++) {
        tmp = z.get_digit(bb, i);
        BigNumber u; u = m_*tmp;
        u = u % bb;
        tmp = u*m;
        for(int j = 0; j < i; j++) {
            tmp = tmp * bb;
        }
        z = z + tmp;
    }
    z = z / R;
    if(z >= m) z = z - m;
    return z;
}

BigNumber EEA(BigNumber a0, BigNumber a1, BigNumber *x) {
    BigNumber x0, x1, zero, q; BASE one = 1;
    x1 = x1 + one;

    while(a0 % a1 != zero) {
        q = a0 / a1;
        BigNumber tmp0 = a0, tmp; tmp = q*a1;
        a0 = a1;
        a1 = tmp0;
        a1 -= tmp;

        tmp0 = x0; tmp = q*x1;
        x0 = x1;
        x1 = tmp0;
        x1 -= tmp;
    }
    *x = x1;
    return a1;
}