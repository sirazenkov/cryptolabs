#include "BigNumber.h"

BigNumber from_left_to_right(BigNumber x, BigNumber y, BigNumber m) {
    BigNumber z; // z = 0

    if(y == z) {
        BASE one = 1;
        z = z + one;
        return z;
    }

    z = x;
    int y_len = BASE_SIZE * y.get_len();
    for(;!y[y_len-1]; y_len--);
    for(int i = y_len-2; i >= 0; i--) {
        z = z*z;
        z = z % m;
        if(y[i]) {
            z = z * x;
            z = z % m;
        }
    }
    return z;
}