#include "BigNumber.h"
#include <iostream>
#include <fstream>

int main() {
    try {
        ifstream input("input.txt");
        ofstream output("output.txt");
        if(!(input.is_open() && output.is_open())) {
            throw("File does not exist!");
        }
        while(!input.eof()) {
            BigNumber x, y, m, z;
            char xs[100], ys[100], ms[100], zs[100];
            bool flag = true;
            char c;

            int i = 0;
            do {
                c = input.get();
                if ((c < '0' || c > '9') && c != ' ') {
                    flag = false;
                } else if(c != ' ') {
                    xs[i++] = c;
                }
            } while (flag && c != ' ');
            if(!flag) continue;
            xs[i] = '\0';

            i = 0;
            do {
                c = input.get();
                if ((c < '0' || c > '9') && c != ' ') {
                    flag = false;
                } else if(c != ' ') {
                    ys[i++] = c;
                }
            } while (flag && c != ' ');
            if(!flag) continue;
            ys[i] = '\0';

            i = 0;
            do {
                c = input.get();
                if ((c < '0' || c > '9') && c != '\n') {
                    flag = false;
                } else if(c != '\n') {
                    ms[i++] = c;
                }
            } while (flag && c != '\n');
            if(!flag) continue;
            ms[i] = '\0';

            x.base_10_input(xs);
            y.base_10_input(ys);
            m.base_10_input(ms);

            BigNumber zero;
            if(m != zero) {
                z = from_left_to_right(x, y, m);
                z.base_10_output(zs);
                output << zs << (char)(0xa);
            }
        }
        input.close();
        output.close();
    } catch(const char *msg) {
        cout << "Error: " << msg << (char)(0xd) << (char)(0xa);
        return 0;
    } catch(...) {
        cout << "Unknown error!\n";
        return 0;
    }
    return 1;
}
