#include "BigNumber.h"
#include <iostream>
#include <fstream>

int main() {
    try {
        ifstream input("input.txt");
        ofstream output("output.txt");
        if(!(input.is_open() && output.is_open())) {
            throw("File does not exist!");
        }
        while(!input.eof()) {
            BigNumber a, n, r;
            char as[100], ns[100], rs[100];
            bool flag = true;
            char c;

            int i = 0;
            do {
                c = input.get();
                if ((c < '0' || c > '9') && c != ' ') {
                    flag = false;
                    while(c != '\n' && !input.eof()) {
                        c = input.get();
                    }
                } else if(c != ' ') {
                    as[i++] = c;
                }
            } while (flag && c != ' ');
            if(!flag) continue;
            as[i] = '\0';

            i = 0;
            do {
                c = input.get();
                if ((c < '0' || c > '9') && c != '\n') {
                    flag = false;
                    while(c != '\n' && !input.eof()) {
                        c = input.get();
                    }
                } else if(c != '\n') {
                    ns[i++] = c;
                }
            } while (flag && c != '\n');
            if(!flag) continue;
            ns[i] = '\0';

            a.base_10_input(as);
            n.base_10_input(ns);

            if(n.get_len() > 1) {
                r = extracting_the_root(a, n);
            } else {
                BASE nb = 0;
                for(int i = 0; ns[i] != '\0'; i++) {
                    nb = nb*10 + (ns[i]-'0');
                }
                r = extracting_the_root(a, nb);
            }
            r.base_10_output(rs);
            output << r << (char) (0xa);
        }
        input.close();
        output.close();
    } catch(const char *msg) {
        cout << "Error: " << msg << '\n';
        return 0;
    } catch(...) {
        cout << "Unknown error!\n";
        return 0;
    }
    return 1;
}