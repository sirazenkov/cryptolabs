#include "BigNumber.h"
#include <iostream>

using namespace std;

    void BigNumber::normalize() {
        int i;
        for(i = maxLen-1; i > 0 && !digits[i]; i--);
        len = i+1;
    }

    int BigNumber::BN_Compare(BigNumber & bn1, BigNumber & bn2) {
        if(bn1.len > bn2.len || (bn1.is_non_negative() && !bn2.is_non_negative())) {
            return 1;
        } else if(bn1.len < bn2.len || (bn2.is_non_negative() && !bn1.is_non_negative())) {
            return -1;
        } else {
            for(int i = bn1.len-1; i >= 0; i--) {
                if (bn1.digits[i] > bn2.digits[i]) {
                    if (bn1.is_non_negative()) {
                        return 1;
                    } else {
                        return -1;
                    }
                } else if (bn1.digits[i] < bn2.digits[i]) {
                    if (bn1.is_non_negative()) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
            }
            return 0;
        }
    }

    BigNumber::BigNumber(int lenMax, bool random_numbers) {
        len = random_numbers ? lenMax : 1;
        maxLen = lenMax;
        digits = new BASE[maxLen];
        if(random_numbers) {
            for(int i = 0; i < len; i++) {
                do {
                    digits[i] = 0;
                    for (int j = 0; j < BASE_SIZE; j++) {
                        digits[i] |= (rand() % 2) << j;
                    }
                } while(i == len-1 && digits[i] == 0);
            }
        } else {
            for (int i = 0; i < maxLen; i++) {
                digits[i] = 0;
            }
            normalize();
        }
    }

    BigNumber::BigNumber(const BigNumber & bn) {
        maxLen = bn.maxLen;
        len = bn.len;
        non_negative = bn.non_negative;
        digits = new BASE[maxLen];
        for(int i = 0; i < maxLen; digits[i] = bn.digits[i], i++);
    }

    BigNumber::~BigNumber() {
        if(digits) {
            delete [] digits;
            digits = nullptr;
        }
    }

    BigNumber BigNumber::operator = (BigNumber const & bn) {
        if(this != &bn) {
            maxLen = bn.maxLen;
            len = bn.len;
            non_negative = bn.non_negative;
            delete [] digits;
            digits = new BASE[maxLen];
            for(int i = 0; i < maxLen; digits[i] = bn.digits[i], i++);
        }
        return *this;
    }

    bool BigNumber::operator > (BigNumber & bn) {
        return BN_Compare(*this, bn) == 1;
    }

    bool BigNumber::operator < (BigNumber & bn) {
        return BN_Compare(*this, bn) == -1;
    }

    bool BigNumber::operator >= (BigNumber & bn) {
        return BN_Compare(*this, bn) != -1;
    }

    bool BigNumber::operator <= (BigNumber & bn) {
        return BN_Compare(*this, bn) != 1;
    }

    bool BigNumber::operator == (BigNumber & bn) {
        return BN_Compare(*this, bn) == 0;
    }

    bool BigNumber::operator == (const BASE & num) {
        return len == 1 && digits[0] == num;
    }


    bool BigNumber::operator != (BigNumber & bn) {
        return BN_Compare(*this, bn) != 0;
    }

    BigNumber BigNumber::operator + (BigNumber & bn) {
        if(!bn.non_negative && non_negative && *this >= bn) {
            BigNumber bn_copy = bn; bn_copy.non_negative = true;
            BigNumber res; res = *this-bn_copy;
            return res;
        }
        else if(!bn.non_negative && non_negative && *this < bn) {
            BigNumber bn_copy = bn; bn_copy.non_negative = true;
            BigNumber res; res = bn_copy-*this;
            res.non_negative = false;
            return res;
        } else if(!non_negative && bn.non_negative && *this < bn) {
            BigNumber this_copy = *this; this_copy.non_negative = true;
            BigNumber res; res = bn-this_copy;
            return res;
        } else if(!non_negative && bn.non_negative && *this >= bn) {
            BigNumber this_copy = *this; this_copy.non_negative = true;
            BigNumber res; res = this_copy-bn;
            res.non_negative = false;
            return res;
        } else if(!non_negative && !bn.non_negative) {
            BigNumber this_copy = *this; this_copy.non_negative = true;
            BigNumber bn_copy = bn; bn_copy.non_negative = true;
            BigNumber res; res = this_copy+bn_copy;
            res.non_negative = false;
            return res;
        }

        int min_len = len < bn.len ? len : bn.len, j;
        BigNumber result(len + bn.len - min_len + 1);
        D_BASE tmp; BASE k = 0;
        for(j = 0; j < min_len; j++) {
            tmp = digits[j] + bn.digits[j] + k;
            result.digits[j] = (BASE)tmp;
            k = (BASE)(tmp >> BASE_SIZE);
        }
        while(j < len) {
            tmp = digits[j] + k;
            result.digits[j] = (BASE)tmp;
            k = (BASE)(tmp >> BASE_SIZE);
            j++;
        }
        while(j < bn.len) {
            tmp = bn.digits[j] + k;
            result.digits[j] = (BASE)tmp;
            k = (BASE)tmp >> BASE_SIZE;
            j++;
        }
        result.digits[j] = k;
        result.len = result.maxLen - !k;
        return result;
    }

    BigNumber BigNumber::operator + (const BASE & num) {
        if(non_negative) {
            BigNumber result(len + 1);
            D_BASE tmp;
            BASE k;
            int j = 0;
            tmp = digits[0] + num;
            result.digits[0] = (BASE) tmp;
            k = (BASE) (tmp >> BASE_SIZE);
            j++;

            while (j < len) {
                tmp = digits[j] + k;
                result.digits[j] = (BASE) tmp;
                k = (BASE) (tmp >> BASE_SIZE);
                j++;
            }

            result.digits[j] = k;
            result.len = result.maxLen - !k;
            return result;
        } else {
            BigNumber this_copy = *this;
            if(this->len == 1) {
                if(digits[0] <= num) {
                    this_copy.digits[0] = num - this_copy.digits[0];
                    this_copy.non_negative = true;
                } else {
                    this_copy.digits[0] = this_copy.digits[0]-num;
                }
                return this_copy;
            } else {
                this_copy.inverse();
                this_copy = this_copy - num;
                this_copy.inverse();
                return this_copy;
            }
        }
    }

    BigNumber BigNumber::operator += (BigNumber & bn) {
        *this = *this + bn;
        return *this;
    }

    BigNumber BigNumber::operator - (BigNumber & bn) {
        if(!bn.non_negative && non_negative) {
            BigNumber bn_copy = bn; bn_copy.non_negative = true;
            BigNumber res; res = *this+bn_copy;
            return res;
        } else if(!non_negative && bn.non_negative) {
            BigNumber this_copy = *this; this_copy.non_negative = true;
            BigNumber res; res = bn+this_copy; res.non_negative = false;
            return res;
        } else if(!non_negative && !bn.non_negative && *this > bn) {
            BigNumber this_copy = *this;
            this_copy.non_negative = true;
            BigNumber bn_copy = bn;
            bn_copy.non_negative = true;
            BigNumber res; res = this_copy - bn_copy;
            res.non_negative = false;
            return res;
        } else if(((!non_negative && !bn.non_negative) || (non_negative && bn.non_negative))&& *this == bn) {
            BigNumber zero;
            return zero;
        } else if(!non_negative && !bn.non_negative && *this < bn) {
            BigNumber this_copy = *this; this_copy.non_negative = true;
            BigNumber bn_copy = bn; bn_copy.non_negative = true;
            BigNumber res; res = bn_copy - this_copy;
            return res;
        } else if(non_negative && bn.non_negative && *this < bn) {
            BigNumber res; res = bn - *this; res.non_negative = false;
            return res;
        }

        BigNumber result(len);
        D_BASE tmp, k = 0; int j;
        for(j = 0; j < bn.len; j++) {
            tmp = b|digits[j];
            tmp -= bn.digits[j]+k;
            result.digits[j] = (BASE)tmp;
            k = !(tmp >> BASE_SIZE);
        }
        for(;j < len; j++) {
            tmp = (digits[j]-k)|b;
            result.digits[j] = (BASE)tmp;
            k = !(tmp >> BASE_SIZE);
        }
        result.len = result.maxLen;
        result.normalize();
        return result;
    }

    BigNumber BigNumber::operator -= (BigNumber & bn) {
        *this = *this - bn;
        return *this;
    }

    BigNumber BigNumber::operator * (const BASE & num) {
        BigNumber result(len + 1);
        BASE k = 0; D_BASE tmp; int j;
        for(j = 0; j < len; j++) {
            tmp = digits[j] * num + k;
            result.digits[j] = (BASE)tmp;
            k = (BASE)(tmp >> BASE_SIZE);
        }
        result.digits[j] = k;
        result.len = result.maxLen - !k;
        return result;
    }

    BigNumber BigNumber::operator * (BigNumber & bn) {
        BigNumber result(len + bn.len);
        if((!non_negative && bn.non_negative) || (non_negative && !bn.non_negative)) {
            result.non_negative = false;
        }

        D_BASE tmp;
        for(int i = 0, j; i < bn.len; i++) {
            if(!bn.digits[i]) continue;
            BASE k = 0;
            for(j = 0; j < len; j++) {
                tmp = digits[j] * bn.digits[i] + result.digits[i+j] + k;
                result.digits[i + j] = (BASE)tmp;
                k = (BASE)(tmp >> BASE_SIZE);
            }
            result.digits[i+j] = k;
        }
        result.len = result.maxLen;
        result.normalize();
        return result;
    }

    BigNumber BigNumber::operator / (const BASE & num) {
        if(num == 0) {
            throw("Division by zero"); // Деление на ноль
        }
        BigNumber result(len);
        BASE r = 0; D_BASE tmp;
        for(int j = 0; j < len; j++) {
            tmp = (r << BASE_SIZE) + digits[len-1-j];
            result.digits[len-1-j] = tmp / num;
            r = tmp % num;
        }
        result.len = result.maxLen;
        result.normalize();
        return result;
    }

    BASE BigNumber::operator % (const BASE & num) {
        if(num == 0) {
            throw("Division by zero");
        }
        if(non_negative) {
            BASE r = 0;
            for (int j = 0; j < len; j++) {
                D_BASE tmp = r * b + digits[len - 1 - j];
                r = tmp % num;
            }
            return r;
        } else {
            BigNumber this_copy = *this;
            while(!this_copy.is_non_negative()) {
                this_copy = this_copy + num;
            }
            return this_copy.digits[0];
        }
    }

    BigNumber BigNumber::operator / (BigNumber & bn) {
        if(bn.len == 1) {
            if(len == 1) {
                BigNumber res(1);
                res.digits[0] = digits[0]/bn.digits[0];
                res.normalize();
                return res;
            }
            BigNumber res; res = *this / bn.digits[0];
            return res;
        }
        BigNumber null;
        if(bn == null) {
            throw("Division by zero"); // Деление на ноль
        }
        if(*this < bn) {
            return null; // Ноль
        }
        if(*this == bn) {
            null.digits[0] = 1;
            return null; // Возвращаем единицу
        }
        BigNumber this_copy;
        this_copy = *this;// Работаем с копией, чтобы не менять исходное число
        BigNumber q(len - bn.len + 1, true); // Для хранения результата
        if((!non_negative && bn.non_negative) || (non_negative && !bn.non_negative)) {
            q.non_negative = false;
        }
        // Нормализация
        BASE d = b / (bn.digits[bn.len - 1] + 1);
        if (d != 1) {
            this_copy = this_copy * d;
            bn = bn * d;
        } else {
            this_copy = BigNumber(len + 1, true);
            for(int i = 0; i < len; i++) {
                this_copy.digits[i] = digits[i];
            }
            this_copy.digits[len] = 0;
        }

        for (int j = len - bn.len; j >= 0; j--) {
            D_BASE q_tmp, r_tmp; // q' и r'
            // Шаг D3
            q_tmp = (((D_BASE)this_copy.digits[bn.len + j] << BASE_SIZE) + this_copy.digits[bn.len + j - 1]) / bn.digits[bn.len - 1];
            r_tmp = (((D_BASE)this_copy.digits[bn.len + j] << BASE_SIZE) + this_copy.digits[bn.len + j - 1]) % bn.digits[bn.len - 1];
            while (q_tmp == b || q_tmp * bn.digits[bn.len - 2] > (b * r_tmp + this_copy.digits[bn.len + j - 2])) {
                q_tmp--;
                r_tmp += bn.digits[bn.len - 1];
                if (r_tmp < b) {
                    continue;
                } else {
                    break;
                }
            }

            BigNumber tmp(bn.len + 1, true);
            for(int i = 0; i < bn.len+1; i++) { // Беру n+1 разрядов делимого
                tmp.digits[i] = this_copy.digits[j+i];
            }
            // Шаги D4-D6
            BigNumber product;
            product = bn*(BASE)q_tmp;
            q.digits[j] = q_tmp; // D5
            if(tmp >= product) {
                tmp -= product;
            } else {
                do {
                    q.digits[j]--; // D6
                    product -= bn;
                } while(tmp < product);
                tmp -= product;
            }

            for(int i = 0; i < bn.len+1; i++) { // Копирую изменившиеся значения разрядов в делимое
                this_copy.digits[j+i] = tmp.digits[i];
            }

        }
        q.normalize();
        return q;
    }

    BigNumber BigNumber::operator % (BigNumber & bn) {
        if(!bn.is_non_negative()) {
            bn.inverse();
        }
        if(!non_negative) {
            BigNumber this_copy = *this, tmp;
            this_copy.inverse();
            tmp = this_copy % bn;
            tmp.inverse();
            tmp = tmp + bn;
            return tmp;
        }
        if(bn.len == 1) {
            if(len == 1) {
                BigNumber res(2);
                res.digits[0] = digits[0] % bn.digits[0];
                res.normalize();
                return res;
            }
            BigNumber res(2);
            res.digits[0] = *this % bn.digits[0];
            res.normalize();
            return res;
        }
        BigNumber null;
        if(bn == null) {
            throw("Division by zero"); // Деление на ноль
        } else if(*this < bn) {
            return *this;
        } else if(*this == bn) {
            return null;
        } else {
            BigNumber this_copy;
            this_copy = *this;// Работаем с копией, чтобы не менять исходное число

            // Нормализация
            BASE d = b / (bn.digits[bn.len - 1] + 1);
            if (d != 1) {
                this_copy = this_copy * d;
                bn = bn * d;
            } else {
                this_copy = BigNumber(len + 1, true);
                for(int i = 0; i < len; i++) {
                    this_copy.digits[i] = digits[i];
                }
                this_copy.digits[len] = 0;
            }

            for (int j = len - bn.len; j >= 0; j--) {
                D_BASE q_tmp, r_tmp; // q' и r'
                // Шаг D3
                q_tmp = (this_copy.digits[bn.len + j] * b + this_copy.digits[bn.len + j - 1]) / bn.digits[bn.len - 1];
                r_tmp = (this_copy.digits[bn.len + j] * b + this_copy.digits[bn.len + j - 1]) % bn.digits[bn.len - 1];
                while (q_tmp == b || q_tmp * bn.digits[bn.len - 2] > (b * r_tmp + this_copy.digits[bn.len + j - 2])) {
                    q_tmp--;
                    r_tmp += bn.digits[bn.len - 1];
                    if (r_tmp < b) {
                        continue;
                    } else {
                        break;
                    }
                }

                BigNumber tmp(bn.len + 1, true);
                for(int i = 0; i < bn.len+1; i++) {
                    tmp.digits[i] = this_copy.digits[j+i]; // Копирую n+1 разрядов делимого
                }
                // Шаги D4-D6
                BigNumber product;
                product = bn*(BASE)q_tmp; // D5
                if(tmp >= product) {
                    tmp -= product;
                } else {
                    do { // D6
                        q_tmp--;
                        product = bn * (BASE)q_tmp;
                    } while(tmp < product);
                    tmp -= product;
                }

                for(int i = 0; i < bn.len+1; i++) { // Копирую изменившиеся значения разрядов в делимое
                    this_copy.digits[j+i] = tmp.digits[i];
                }

            }

            this_copy = this_copy / d; // Денормализация

            BigNumber result(bn.len);
            for(int i = 0; i < bn.len; i++) { // Беру младшие n разрядов
                result.digits[i] = this_copy.digits[i];
            }
            result.len = result.maxLen;
            result.normalize();
            return result;
        }
    }

    BigNumber BigNumber::base_10_input(const char *string) {
        if(string) {
            maxLen = 1;
            len = 1;
            delete[] digits;
            digits = new BASE[maxLen];
            digits[0] = 0;
            for (int j = 0, str_len = strlen(string); j < str_len; j++) {
                if(string[j] < '0' || string[j] > '9') {
                    throw("Invalid input!"); // Некорректный ввод
                }
                BASE base = 10, digit = string[j] - '0'; // Беру очередной разряд
                *this = *this * base + digit;
            }
        } return *this;
    }

    void BigNumber::base_10_output(char* string) {
        char tmp_str[200]; // Временный массив для символов
        BigNumber bn = *this, zero; int j;
        if(bn == zero) {
            string[0] = '0';
            string[1] = '\0';
            return;
        }
        for(j = 199; bn != zero; j--) { // Записываю символы с конца массива символов
            BASE digit; digit = bn % (BASE)10;
            tmp_str[j] = digit + '0';
            bn = bn / (BASE)10;
        }
        j++;
        int k;
        for(k = 0; j <= 199; j++, k++) {
            string[k] = tmp_str[j]; // Копирую строку в исходный указатель
        }
        string[k] = '\0'; // Добавляю нулевой байт
        return;
    }

    int BigNumber::get_len() {
        return len;
    }

    int BigNumber::get_maxLen() {
        return maxLen;
    }

    BASE BigNumber::operator [] (int i) {
        return (digits[i / BASE_SIZE] >> (i % BASE_SIZE)) & 1;
    }

    int BigNumber::hamming_weight() {
        int count = 0;
        for(int i = len*BASE_SIZE - 1; i >= 0; i--) {
            if((*this)[i]) {
                count++;
            }
        }
        return count;
    }

    int BigNumber::number_length(BigNumber & bb) {
        BigNumber one_b; BASE one = 1;
        one_b = one_b + one;
        if(bb <= one_b) {
            throw "Invalid base!";
        }
        BigNumber this_copy = *this, zero;
        if(this_copy == zero)
            return 1;
        int count = 0;
        while(this_copy > zero) {
            this_copy = this_copy / bb;
            count++;
        }
        return count;
    }

    int BigNumber::msb() {
        int i;
        for(i = len*BASE_SIZE-1; i > 0; i--) {
            if((*this)[i]) {
                return i+1;
            }
        }
        return i+1;
    }

    bool BigNumber::lsb() {
        return digits[0] & 1;
    }

    void BigNumber::inverse() {
        non_negative = not(non_negative);
    }

    BigNumber::operator int() {
        int result = 0;
        for(int i = 0; i < sizeof(int)*8/BASE_SIZE && i < len; i++) {
            result += digits[i] << i*BASE_SIZE;
        }
        return result;
    }

    BigNumber BigNumber::get_digit(BigNumber base, int num) {
        BigNumber this_copy = *this, res;
        for(int i = 0; i < num; i++) {
            this_copy = this_copy / base;
        }
        res = this_copy % base;
        return res;
    }

    BigNumber BigNumber::operator ++ () {
        BASE one = 1;
        *this = *this + one;
        return *this;
    }

    BigNumber pow(BigNumber &a, BigNumber &n) {
        BigNumber result, zero; result = result + (BASE)1;
        while(n > zero) {
            result = result * a;
            n = n - 1;
        }
        return result;
    }

    BigNumber legendre(BigNumber &a, BigNumber &p) {
        BASE oneb = 1;
        BigNumber one, zero; one = one + oneb;
        if(a == one) {
            return one;
        }
        else if(a % p == zero) {
            return zero;
        }
        BigNumber result = one;
        const int primes_q = 4;
        BigNumber two, three, five, seven; two = one + one, three = two + one, five = two + three, seven = five + two;
        BigNumber m_one = one; m_one.inverse();
        BigNumber primes[primes_q] = {two,three,five,seven};
        BigNumber prime_exponents[primes_q] = {zero,zero,zero,zero};
        BigNumber a_copy = a, tmp, tmp2;
        if(a < zero) {
            tmp = p-oneb; tmp = tmp / (BASE)2;
            tmp = pow(m_one, tmp) % p;
            result = result * tmp;
            a_copy.inverse();
        }
        for(int i = 0; i < primes_q; i++) {
            while(a_copy > one && a_copy % primes[i] == zero) {
                a_copy = a_copy / primes[i];
                prime_exponents[i] = prime_exponents[i] + oneb;
            }
            if(prime_exponents[i] == zero || prime_exponents[i] % (BASE)2 == zero) {
                continue;
            } else if(primes[i] == two) {
                tmp2 = (p*p-oneb)/(seven+oneb);
                tmp = pow(m_one, tmp2);
                result = result * tmp;
            } else {
                tmp = primes[i]-oneb;
                tmp2 = (p-oneb)/(three+oneb);
                tmp = tmp * tmp2;
                tmp = pow(m_one, tmp);
                tmp2 = p % primes[i];
                tmp2 = legendre(tmp2, primes[i]);
                tmp = tmp * tmp2;
                result = result * tmp;
            }
        }
        return result;
    }