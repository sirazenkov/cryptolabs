#include "BigNumber.h"
#include <iostream>
#include <fstream>

int main() {
    try {
        ifstream input("input.txt");
        ofstream output("output.txt");
        if(!(input.is_open() && output.is_open())) {
            throw("File does not exist!");
        }
        while(!input.eof()) {
            BigNumber n;
            char ns[100], outs[100];
            bool flag = true;
            char c;

            int i = 0;
            do {
                c = input.get();
                if ((c < '0' || c > '9') && c != '\n') {
                    flag = false;
                    while(c != '\n' && !input.eof()) {
                        c = input.get();
                    }
                } else if(c != '\n') {
                    ns[i++] = c;
                }
            } while (flag && c != '\n');
            if(!flag) continue;
            ns[i] = '\0';

            n.base_10_input(ns);

            BASE one = 1, two = 2;
            BigNumber btwo; btwo = btwo + two;

            while (!n.lsb()) {
                n = n / btwo;
            }
            BigNumber x, a, b; x = extracting_the_root(n, two);
            if(x * x == n) {
                    a = b = x;
            } else {
                BigNumber bnine; bnine = btwo + btwo + btwo + btwo + one;
                BigNumber bthree, bseven, bten; bthree = btwo + one; bseven = bnine - btwo; bten = bseven + bthree;
                BigNumber tmp, tmp2;
                const int module_q = 3;
                BigNumber modules[module_q] = {bthree, bseven, bten};
                int s[module_q][10];
                for (int i = 0; i < module_q; i++) {
                    BigNumber j;
                    for (BigNumber zero; j < modules[i]; j = j + one) {
                        tmp = j * j;
                        tmp2 = tmp - n;
                        tmp = tmp2 % modules[i];
                        tmp2 = legendre(tmp, modules[i]);
                        if (tmp % modules[i] == zero || tmp2 >= zero) {
                            s[i][int(j)] = 1;
                        } else {
                            s[i][int(j)] = 0;
                        }
                    }
                }

                BigNumber r[3];
                x = x + one;
                for(int i = 0; i < module_q; i++) {
                    tmp = x % modules[i];
                    r[i] = tmp;
                }

                BASE six = 6;
                BigNumber border;
                border = (n + bnine);
                border = border / six;
                bool cont = false;

                while (true) {
                    int quadratic = 1;
                    for(int i = 0; i < module_q; i++) {
                        quadratic &= s[i][r[i]];
                    }
                    if(quadratic == 1) {
                        BigNumber y, z;
                        z = x*x - n;
                        y = extracting_the_root(z, two);
                        if(y*y == z) {
                            a = x + y;
                            b = x - y;
                            break;
                        }
                    }
                    x = x + one;
                    if (x > border) {
                        cont = true;
                        break;
                    } else {
                        for(int i = 0; i < module_q; i++) {
                            r[i] = (r[i] + one) % modules[i];
                        }
                    }
                }
                if (cont) {
                    continue;
                }
            }
            if(a != n && b != n) {
                a.base_10_output(outs);
                output << outs << ' ';
                b.base_10_output(outs);
                output << outs << char(0xa);
            }
        }
        input.close();
        output.close();
    } catch(const char *msg) {
        cout << "Error: " << msg << '\n';
        return 0;
    } catch(...) {
        cout << "Unknown error!\n";
        return 0;
    }
    return 1;
}

