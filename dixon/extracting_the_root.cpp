#include "BigNumber.h"

BigNumber extracting_the_root(BigNumber &a, BigNumber &n) {
    BigNumber a0, a1 = a;
    do {
        a0 = a1;
        BASE tmpb = 1; BigNumber tmp; tmp = tmp + tmpb;
        for(BigNumber i = tmp; i < n; i = i + tmpb) {
            tmp = tmp * a0;
        }
        if(tmp.get_len() > 1) {
            a1 = a / tmp;
        } else {
            tmpb = tmp.digits[0];
            a1 = a / tmpb;
        }
        tmp = n * a0;
        tmp = tmp - a0;
        a1 = a1 + tmp;
        a1 = a1 / n;
    } while(a0 > a1);
    return a0;
}

BigNumber extracting_the_root(BigNumber &a, const BASE & n) {
    BigNumber a0, a1 = a;
    do {
        a0 = a1;
        BASE tmpb = 1; BigNumber tmp; tmp = tmp + tmpb;
        for(BASE i = 1; i < n; i++) {
            tmp = tmp * a0;
        }
        if(tmp.get_len() > 1) {
            a1 = a / tmp;
        } else {
            tmpb = tmp.digits[0];
            a1 = a / tmpb;
        }
        tmp = a0 * n;
        tmp = tmp - a0;
        a1 = a1 + tmp;
        a1 = a1 / n;
        a1.normalize();
        a0.normalize();
    } while(a1 < a0);
    return a0;
}