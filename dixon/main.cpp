#include "BigNumber.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <time.h>

int main() {
    try {
        ifstream input("input.txt");
        ofstream output("output.txt");
        if(!(input.is_open() && output.is_open())) {
            throw("File does not exist!");
        }
        srand(time(NULL));
        while(!input.eof()) {
                // Read input
            //--------------------------------------------------------
            BigNumber n;
            int k;
            char ns[100], ks[100], ds[100];
            bool flag = true;
            char c;

            int i = 0;
            do {
                c = input.get();
                if ((c < '0' || c > '9') && c != ' ') {
                    flag = false;
                    while(c != '\n' && !input.eof()) {
                        c = input.get();
                    }
                } else if(c != ' ') {
                    ns[i++] = c;
                }
            } while (flag && c != ' ');
            if(!flag) continue;
            ns[i] = '\0';

            i = 0;
            do {
                c = input.get();
                if ((c < '0' || c > '9') && c != '\n') {
                    flag = false;
                    while(c != '\n' && !input.eof()) {
                        c = input.get();
                    }
                } else if(c != '\n') {
                    ks[i++] = c;
                }
            } while (flag && c != '\n');
            if(!flag) continue;
            ks[i] = '\0';
            n.base_10_input(ns);
            k = atoi(ks);
            if(k < 2) {
                continue;
            }
            //--------------------------------------------------------
            //cout << "ns = " << ns << "; k = " << k << '\n';

                // Check and validate input
            //--------------------------------------------------------
            BigNumber two, tmp, zero; two = two + (BASE)2;
            BigNumber k_b; k_b = BigNumber::int2BigNumber(k);
            tmp = extracting_the_root(n, two);
            if(k_b >= tmp) {
                continue;
            }
            BigNumber one; one = one + (BASE)1;
            while(n % two == zero) {
                n = n / two;
            }
            if(n == one || n.isPrime() || n.isPower()) {
                continue;
            }
            //--------------------------------------------------------
            //cout << "n = " << n << "; k = " << k << '\n';

            // Generate k prime numbers
            //--------------------------------------------------------
            BigNumber *primes = new BigNumber[k];
            BigNumber bnum; bnum = bnum + (BASE)2;
            primes[0] = bnum;
            for(int i = 1; i < k;) {
                bnum = bnum + (BASE)1;
                flag = false;
                for(int j = 0; j < i; j++) {
                    if(bnum % primes[j] == zero) {
                        flag = true;
                        break;
                    }
                }
                if(flag) {
                    continue;
                }
                primes[i++] = bnum;
            }
            //--------------------------------------------------------
            //cout << "Prime numbers generated\n";

                // Generate a_i, b_i, e_ij
            //--------------------------------------------------------
            int t = k + 1;
            vector<BigNumber> a_arr(t), b_arr(t);
            vector<vector<BigNumber>>e_arr;
            e_arr.resize(t);
            for(int i = 0; i < t;) {
                e_arr[i].resize(k);
                BigNumber a(1,true), b, b_copy;
                a = a % n; b = a*a; b = b % n;
                if(b == zero) {
                    continue;
                }
                b_copy = b;
                for(int j = 0; j < k; j++) {
                    e_arr[i][j] = zero;
                }
                for(int j = 0; j < k; j++) {
                    while(b_copy % primes[j] == zero) {
                        b_copy = b_copy / primes[j];
                        e_arr[i][j] = e_arr[i][j] + one;
                    }
                }
                if(b_copy == one) {
                    a_arr[i] = a;
                    b_arr[i++] = b;
                }
            }
            //--------------------------------------------------------
            //cout << "a_i, b_i, e_ij generated\n";

                // Find solution
            //--------------------------------------------------------
            flag = false;
            while(true) {
                    // Build matrix V
                //-----------------------------------------
                BASE V_mtx[t][k];
                for (int i = 0; i < t; i++) {
                    for (int j = 0; j < k; j++) {
                        V_mtx[i][j] = e_arr[i][j] % (BASE)2;
                    }
                }
                //-----------------------------------------
                //cout << "Built matrix V\n";

                    // Calculate border for solutions
                //-----------------------------------------
                BigNumber c_arr, border; c_arr = one;
                border = one;
                for (int i = 0; i < t; i++) {
                    border = border * two;
                }
                //-----------------------------------------

                    // Solutions enumeration
                //-----------------------------------------
                for (; c_arr < border; c_arr = c_arr + one) {
                    BASE cV[k];
                    for(int i = 0; i < k; i++) {
                        cV[i] = (BASE)0;
                    }
                    BigNumber c_arr_copy;
                    for(int i = 0; i < k; i++) {
                        c_arr_copy = c_arr;
                        for(int j = 0; j < t; j++) {
                            if (c_arr_copy.lsb()) {
                                cV[i] = cV[i] + V_mtx[j][i];
                            }
                            c_arr_copy = c_arr_copy / two;
                        }
                        cV[i] = cV[i] % two;
                    }
                    flag = false;
                    for(int i = 0; i < k; i++) {
                        if(cV[i] == one) {
                            flag = true;
                            break;
                        }
                    }
                    if(flag) {
                        continue;
                    }
                    //cout << "c_arr = " << c_arr << '\n';
                    //cout << "Checking solution\n";
                    c_arr_copy = c_arr;
                    BigNumber x, y;
                    x = one; y = one;
                    for (int i = 0; i < t; i++) {
                        if (c_arr_copy.lsb()) {
                            x = x * a_arr[i];
                            x = x % n;
                            for (int j = 0; j < k; j++) {
                                tmp = pow(primes[j], e_arr[i][j]);
                                y = y * tmp;
                            }
                        }
                        c_arr_copy = c_arr_copy / two;
                    }
                    y = extracting_the_root(y, (BASE) 2);
                    y = y % n;
                    //cout << "Calculating solution\n";
                    if (x == y) {
                        //cout << "Invalid solution\n";
                        continue;
                    }
                    y.inverse();
                    while(!y.is_non_negative()) {
                        y = y + n;
                    }
                    if (x == y) {
                        //cout << "Invalid solution\n";
                        continue;
                    }
                    flag = true;
                    BigNumber d;
                    //cout << "Starting final calculation\n";
                    tmp = x - y;
                    while(!tmp.is_non_negative()) {
                        tmp = tmp + n;
                    }
                    d = EEA(tmp, n, &tmp);
                    if(d == one || d == n) {
                        tmp = x + y;
                        d = EEA(tmp, n, &tmp);
                    }
                    //cout << "Calculated solution\n";
                    d.base_10_output(ds);
                    //cout << "n = " << n << "; ds = " << ds << '\n';
                    output << ds << (char) (0xa);
                    break;
                }
                //-----------------------------------------

                // If found solution or invalid input
                if (flag) {
                    break;
                }

                    // Solution not found, extension
                //-----------------------------------------
                t++;
                a_arr.resize(t);
                b_arr.resize(t);
                e_arr.resize(t);
                e_arr[t-1].resize(k);
                while(true) {
                    BigNumber a(1, true), b, b_copy;
                    a = a % n;
                    b = a * a;
                    b = b % n;
                    if(b == zero) {
                        continue;
                    }
                    b_copy = b;
                    for (int j = 0; j < k; j++) {
                        e_arr[t - 1][j] = zero;
                    }
                    for (int j = 0; j < k; j++) {
                        while (b_copy % primes[j] == zero) {
                            b_copy = b_copy / primes[j];
                            e_arr[t - 1][j] = e_arr[t - 1][j] + one;
                        }
                    }
                    if (b_copy == one) {
                        a_arr[t - 1] = a;
                        b_arr[t - 1] = b;
                        break;
                    }
                }
            }
            delete [] primes;
            //--------------------------------------------------------
        }
        input.close();
        output.close();
    } catch(const char *msg) {
        cout << "Error: " << msg << '\n';
        return 0;
    } catch(...) {
        cout << "Unknown error!\n";
        return 0;
    }
    return 1;
}