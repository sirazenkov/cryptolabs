#include "BigNumber.h"
#include <iostream>
#include <fstream>
#include <vector>

int main() {
    try {
        ifstream input("input.txt");
        ofstream output("output.txt");
        if(!(input.is_open() && output.is_open())) {
            throw("File does not exist!");
        }
        while(!input.eof()) {
            BigNumber n, B;
            char ns[100], Bs[100], ds[100];
            bool flag = true;
            char c;

            int i = 0;
            do {
                c = input.get();
                if ((c < '0' || c > '9') && c != ' ') {
                    flag = false;
                    while(c != '\n' && !input.eof()) {
                        c = input.get();
                    }
                } else if(c != ' ') {
                    ns[i++] = c;
                }
            } while (flag && c != ' ');
            if(!flag) continue;
            ns[i] = '\0';

            i = 0;
            do {
                c = input.get();
                if ((c < '0' || c > '9') && c != '\n') {
                    flag = false;
                    while(c != '\n' && !input.eof()) {
                        c = input.get();
                    }
                } else if(c != '\n') {
                    Bs[i++] = c;
                }
            } while (flag && c != '\n');
            if(!flag) continue;
            Bs[i] = '\0';

            n.base_10_input(ns);
            B.base_10_input(Bs);

            int complete;
            BASE two = 2; BigNumber btwo; btwo = btwo + two;
            BigNumber divisor, prev_divisor;
            BASE three = 3, five = 5, seven = 7, six = 6, one = 1;
            BigNumber zero, bone; bone = bone + one;
            divisor = divisor + three;
            flag = false;

            while(!n.lsb() ) {
                n = n / btwo;
                btwo.base_10_output(ds);
                output << ds << ' ';
            }

            while(true) {
                if (n == bone) {
                    n.base_10_output(ds);
                    output << ds << ' ';
                    complete = 0;
                    flag = true;
                } else {
                    bool inner_flag = false;
                    while(true) {
                        if(divisor > B) {
                            n.base_10_output(ds);
                            output << ds << ' ';
                            complete = 1;
                            flag = true; inner_flag = true;
                        } else {
                            BigNumber r, q, tmp;
                            r = n % divisor;
                            q = n / divisor;
                            if (r == zero) {
                                divisor.base_10_output(ds);
                                output << ds << ' ';
                                n = q;
                                inner_flag = true;
                            } else if (divisor < q) {
                                if (divisor == three) {
                                    divisor = zero + five;
                                } else if (divisor == five) {
                                    divisor = zero + seven;
                                } else {
                                    if (divisor == seven) {
                                        prev_divisor = zero + five;
                                    }
                                    tmp = divisor;
                                    divisor = prev_divisor + six;
                                    prev_divisor = tmp;
                                }
                            } else {
                                n.base_10_output(ds);
                                output << ds << ' ';
                                bone.base_10_output(ds);
                                output << ds << ' ';
                                complete = 0;
                                flag = true;
                                inner_flag = true;
                            }
                        }
                            if(inner_flag) {
                                break;
                            }
                        }
                }
                if(flag) {
                    break;
                }
            }
            output << complete << char(0xa);
        }
        input.close();
        output.close();
    } catch(const char *msg) {
        cout << "Error: " << msg << '\n';
        return 0;
    } catch(...) {
        cout << "Unknown error!\n";
        return 0;
    }
    return 1;
}