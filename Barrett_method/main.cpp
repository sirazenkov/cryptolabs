#include "BigNumber.h"
#include <iostream>
#include <fstream>

int main() {
    try {
        ifstream input("input.txt");
        ofstream output("output.txt");
        if(!(input.is_open() && output.is_open())) {
            throw("File does not exist!");
        }
        while(!input.eof()) {
            BigNumber x, m, r;
            char xs[100], ms[100], rs[100];
            bool flag = true;
            char c;

            int i = 0;
            do {
                c = input.get();
                if ((c < '0' || c > '9') && c != ' ') {
                    flag = false;
                    while(c != '\n' && !input.eof()) {
                        c = input.get();
                    }
                } else if(c != ' ') {
                    xs[i++] = c;
                }
            } while (flag && c != ' ');
            if(!flag) continue;
            xs[i] = '\0';

            i = 0;
            do {
                c = input.get();
                if ((c < '0' || c > '9') && c != '\n') {
                    flag = false;
                    while(c != '\n' && !input.eof()) {
                        c = input.get();
                    }
                } else if(c != '\n') {
                    ms[i++] = c;
                }
            } while (flag && c != '\n');
            if(!flag) continue;
            ms[i] = '\0';

            x.base_10_input(xs);
            m.base_10_input(ms);

            int x_len, m_len;
            for(i = x.get_len()*BASE_SIZE-1; x[i] == 0 && i > 0; i--);
            i++;
            if(i % 2) {
                x_len = (i+1)/2;
            } else {
                x_len = i/2;
            }
            for(i = m.get_len()*BASE_SIZE-1; m[i] == 0 && i > 0; i--);
            i++;
            if(i % 2) {
                m_len = (i+1)/2;
            } else {
                m_len = i/2;
            }

            if(x_len > 2*m_len) {
                continue;
            }

            BigNumber zero;
            if(m != zero) {
                r = barrett(x, m, m_len);
                r.base_10_output(rs);
                output << rs << (char)(0xa);
            }
        }
        input.close();
        output.close();
    } catch(const char *msg) {
        cout << "Error: " << msg << '\n';
        return 0;
    } catch(...) {
        cout << "Unknown error!\n";
        return 0;
    }
    return 1;
}
