#include "BigNumber.h"
#include <iostream>

using namespace std;

    void BigNumber::normalize() {
        int i;
        for(i = maxLen-1; i > 0 && !digits[i]; i--);
        len = i+1;
    }

    int BigNumber::BN_Compare(BigNumber & bn1, BigNumber & bn2) {
        if(bn1.len > bn2.len) {
            return 1;
        } else if(bn1.len < bn2.len) {
            return -1;
        } else {
            for(int i = bn1.len-1; i >= 0; i--) {
                if(bn1.digits[i] > bn2.digits[i]) {
                    return 1;
                } else if(bn1.digits[i] < bn2.digits[i]) {
                    return -1;
                }
            }
            return 0;
        }
    }

    BigNumber::BigNumber(int lenMax, bool random_numbers) {
        len = random_numbers ? lenMax : 1;
        maxLen = lenMax;
        digits = new BASE[maxLen];
        if(random_numbers) {
            for(int i = 0; i < len; i++) {
                do {
                    digits[i] = 0;
                    for (int j = 0; j < BASE_SIZE; j++) {
                        digits[i] |= (rand() % 2) << j;
                    }
                } while(i == len-1 && digits[i] == 0);
            }
        } else {
            for (int i = 0; i < maxLen; i++) {
                digits[i] = 0;
            }
            normalize();
        }
    }

    BigNumber::BigNumber(BigNumber & bn) {
        maxLen = bn.maxLen;
        len = bn.len;
        digits = new BASE[maxLen];
        for(int i = 0; i < maxLen; digits[i] = bn.digits[i], i++);
    }

    BigNumber::~BigNumber() {
        if(digits) {
            delete [] digits;
            digits = NULL;
        }
    }

    BigNumber BigNumber::operator = (BigNumber const & bn) {
        if(this != &bn) {
            maxLen = bn.maxLen;
            len = bn.len;
            delete [] digits;
            digits = new BASE[maxLen];
            for(int i = 0; i < maxLen; digits[i] = bn.digits[i], i++);
        }
        return *this;
    }

    bool BigNumber::operator > (BigNumber & bn) {
        return BN_Compare(*this, bn) == 1;
    }

    bool BigNumber::operator < (BigNumber & bn) {
        return BN_Compare(*this, bn) == -1;
    }

    bool BigNumber::operator >= (BigNumber & bn) {
        return BN_Compare(*this, bn) != -1;
    }

    bool BigNumber::operator <= (BigNumber & bn) {
        return BN_Compare(*this, bn) != 1;
    }

    bool BigNumber::operator == (BigNumber & bn) {
        return BN_Compare(*this, bn) == 0;
    }

    bool BigNumber::operator != (BigNumber & bn) {
        return BN_Compare(*this, bn) != 0;
    }

    BigNumber BigNumber::operator + (BigNumber & bn) {
        int min_len = len < bn.len ? len : bn.len, j;
        BigNumber result(len + bn.len - min_len + 1);
        D_BASE tmp; BASE k = 0;
        for(j = 0; j < min_len; j++) {
            tmp = digits[j] + bn.digits[j] + k;
            result.digits[j] = (BASE)tmp;
            k = (BASE)(tmp >> BASE_SIZE);
        }
        while(j < len) {
            tmp = digits[j] + k;
            result.digits[j] = (BASE)tmp;
            k = (BASE)(tmp >> BASE_SIZE);
            j++;
        }
        while(j < bn.len) {
            tmp = bn.digits[j] + k;
            result.digits[j] = (BASE)tmp;
            k = (BASE)tmp >> BASE_SIZE;
            j++;
        }
        result.digits[j] = k;
        result.len = result.maxLen - !k;
        return result;
    }

    BigNumber BigNumber::operator + (BASE & num) {
        BigNumber result(len + 1);
        D_BASE tmp; BASE k = 0; int j = 0;
        tmp = digits[0] + num;
        result.digits[0] = (BASE)tmp;
        k = (BASE)(tmp >> BASE_SIZE);
        j++;

        while(j < len) {
            tmp = digits[j] + k;
            result.digits[j] = (BASE)tmp;
            k = (BASE)(tmp >> BASE_SIZE);
            j++;
        }

        result.digits[j] = k;
        result.len = result.maxLen - !k;
        return result;
    }

    BigNumber BigNumber::operator += (BigNumber & bn) {
        *this = *this + bn;
        return *this;
    }

    BigNumber BigNumber::operator - (BigNumber & bn) {
        if(*this < bn)
            throw("Invalid operand in -");
        BigNumber result(len);
        D_BASE tmp, k = 0; int j;
        for(j = 0; j < bn.len; j++) {
            tmp = b|digits[j];
            tmp -= bn.digits[j]+k;
            result.digits[j] = (BASE)tmp;
            k = !(tmp >> BASE_SIZE);
        }
        for(;j < len; j++) {
            tmp = (digits[j]-k)|b;
            result.digits[j] = (BASE)tmp;
            k = !(tmp >> BASE_SIZE);
        }
        result.len = result.maxLen;
        result.normalize();
        return result;
    }

    BigNumber BigNumber::operator -= (BigNumber & bn) {
        *this = *this - bn;
        return *this;
    }

    BigNumber BigNumber::operator * (BASE num) {
        BigNumber result(len + 1);
        BASE k = 0; D_BASE tmp; int j;
        for(j = 0; j < len; j++) {
            tmp = digits[j] * num + k;
            result.digits[j] = (BASE)tmp;
            k = (BASE)(tmp >> BASE_SIZE);
        }
        result.digits[j] = k;
        result.len = result.maxLen - !k;
        return result;
    }

    BigNumber BigNumber::operator * (BigNumber & bn) {
        BigNumber result(len + bn.len);

        D_BASE tmp;
        for(int i = 0, j; i < bn.len; i++) {
            if(!bn.digits[i]) continue;
            BASE k = 0;
            for(j = 0; j < len; j++) {
                tmp = digits[j] * bn.digits[i] + result.digits[i+j] + k;
                result.digits[i + j] = (BASE)tmp;
                k = (BASE)(tmp >> BASE_SIZE);
            }
            result.digits[i+j] = k;
        }
        result.len = result.maxLen;
        result.normalize();
        return result;
    }

    BigNumber BigNumber::operator / (BASE num) {
        if(num == 0) {
            throw("Division by zero"); // Деление на ноль
        }
        BigNumber result(len);
        BASE r = 0; D_BASE tmp; int j;
        for(int j = 0; j < len; j++) {
            tmp = (r << BASE_SIZE) + digits[len-1-j];
            result.digits[len-1-j] = tmp / num;
            r = tmp % num;
        }
        result.len = result.maxLen;
        result.normalize();
        return result;
    }

    BASE BigNumber::operator % (BASE num) {
        if(num == 0) {
            throw("Division by zero");
        }
        BASE r = 0;
        for(int j = 0; j < len; j++) {
            D_BASE tmp = r*b + digits[len-1-j];
            r = tmp % num;
        }
        return r;
    }

    BigNumber BigNumber::operator / (BigNumber bn) {
        BigNumber null;
        if(bn == null) {
            throw("Division by zero"); // Деление на ноль
        }
        if(*this < bn) {
            return null; // Ноль
        }
        if(*this == bn) {
            null.digits[0] = 1;
            return null; // Возвращаем единицу
        }
        BigNumber this_copy;
        this_copy = *this;// Работаем с копией, чтобы не менять исходное число
        BigNumber q(len - bn.len + 1, true); // Для хранения результата

        // Нормализация
        BASE d = b / (bn.digits[bn.len - 1] + 1);
        if (d != 1) {
            this_copy = this_copy * d;
            bn = bn * d;
        } else {
            this_copy = BigNumber(len + 1, true);
            for(int i = 0; i < len; i++) {
                this_copy.digits[i] = digits[i];
            }
            this_copy.digits[len] = 0;
        }

        for (int j = len - bn.len; j >= 0; j--) {
            D_BASE q_tmp, r_tmp; // q' и r'
            // Шаг D3
            q_tmp = (((D_BASE)this_copy.digits[bn.len + j] << BASE_SIZE) + this_copy.digits[bn.len + j - 1]) / bn.digits[bn.len - 1];
            r_tmp = (((D_BASE)this_copy.digits[bn.len + j] << BASE_SIZE) + this_copy.digits[bn.len + j - 1]) % bn.digits[bn.len - 1];
            while (q_tmp == b || q_tmp * bn.digits[bn.len - 2] > (b * r_tmp + this_copy.digits[bn.len + j - 2])) {
                q_tmp--;
                r_tmp += bn.digits[bn.len - 1];
                if (r_tmp < b) {
                    continue;
                } else {
                    break;
                }
            }

            BigNumber tmp(bn.len + 1, true);
            for(int i = 0; i < bn.len+1; i++) { // Беру n+1 разрядов делимого
                tmp.digits[i] = this_copy.digits[j+i];
            }
            // Шаги D4-D6
            BigNumber product;
            product = bn*q_tmp;
            q.digits[j] = q_tmp; // D5
            if(tmp >= product) {
                tmp -= product;
            } else {
                do {
                    q.digits[j]--; // D6
                    product -= bn;
                } while(tmp < product);
                tmp -= product;
            }

            for(int i = 0; i < bn.len+1; i++) { // Копирую изменившиеся значения разрядов в делимое
                this_copy.digits[j+i] = tmp.digits[i];
            }

        }
        return q;
    }

    BigNumber BigNumber::operator % (BigNumber bn) {
        BigNumber null;
        if(bn == null) {
            throw("Division by zero"); // Деление на ноль
        } else if(*this < bn) {
            return *this;
        } else if(*this == bn) {
            return null;
        } else {
            BigNumber this_copy;
            this_copy = *this;// Работаем с копией, чтобы не менять исходное число

            // Нормализация
            BASE d = b / (bn.digits[bn.len - 1] + 1);
            if (d != 1) {
                this_copy = this_copy * d;
                bn = bn * d;
            } else {
                this_copy = BigNumber(len + 1, true);
                for(int i = 0; i < len; i++) {
                    this_copy.digits[i] = digits[i];
                }
                this_copy.digits[len] = 0;
            }

            for (int j = len - bn.len; j >= 0; j--) {
                D_BASE q_tmp, r_tmp; // q' и r'
                // Шаг D3
                q_tmp = (this_copy.digits[bn.len + j] * b + this_copy.digits[bn.len + j - 1]) / bn.digits[bn.len - 1];
                r_tmp = (this_copy.digits[bn.len + j] * b + this_copy.digits[bn.len + j - 1]) % bn.digits[bn.len - 1];
                while (q_tmp == b || q_tmp * bn.digits[bn.len - 2] > (b * r_tmp + this_copy.digits[bn.len + j - 2])) {
                    q_tmp--;
                    r_tmp += bn.digits[bn.len - 1];
                    if (r_tmp < b) {
                        continue;
                    } else {
                        break;
                    }
                }

                BigNumber tmp(bn.len + 1, true);
                for(int i = 0; i < bn.len+1; i++) {
                    tmp.digits[i] = this_copy.digits[j+i]; // Копирую n+1 разрядов делимого
                }
                // Шаги D4-D6
                BigNumber product;
                product = bn*q_tmp; // D5
                if(tmp >= product) {
                    tmp -= product;
                } else {
                    do { // D6
                        q_tmp--;
                        product = bn * q_tmp;
                    } while(tmp < product);
                    tmp -= product;
                }

                for(int i = 0; i < bn.len+1; i++) { // Копирую изменившиеся значения разрядов в делимое
                    this_copy.digits[j+i] = tmp.digits[i];
                }

            }

            this_copy = this_copy / d; // Денормализация

            BigNumber result(bn.len);
            for(int i = 0; i < bn.len; i++) { // Беру младшие n разрядов
                result.digits[i] = this_copy.digits[i];
            }
            result.len = result.maxLen;
            result.normalize();
            return result;
        }
    }

    BigNumber BigNumber::base_10_input(char *string) {
        if(string) {
            maxLen = 1;
            len = 1;
            delete[] digits;
            digits = new BASE[maxLen];
            digits[0] = 0;
            for (int j = 0, str_len = strlen(string); j < str_len; j++) {
                if(string[j] < '0' || string[j] > '9') {
                    throw("Invalid input!"); // Некорректный ввод
                }
                BASE base = 10, digit = string[j] - '0'; // Беру очередной разряд
                //cout << "digit = " << digit << '\n';
                *this = *this * base + digit;
            }
        } return *this;
    }

    void BigNumber::base_10_output(char* string) {
        char tmp_str[100]; // Временный массив для символов
        BigNumber bn = *this, zero; int j;

        if(bn == zero) {
            string[0] = '0';
            string[1] = '\0';
            return;
        }

        for(j = 100-1; bn != zero; j--) { // Записываю символы с конца массива символов
            BASE digit = bn % 10;
            tmp_str[j] = digit + '0';
            bn = bn / 10;
        }
        j++;
        int k;
        for(k = 0; j < 100; j++, k++) {
            string[k] = tmp_str[j]; // Копирую строку в исходный указатель
        }
        string[k] = '\0'; // Добавляю нулевой байт
        return;
    }

    int BigNumber::get_len() {
        return len;
    }

    int BigNumber::get_maxLen() {
        return maxLen;
    }

    BASE BigNumber::operator [] (int i) {
        return (digits[i / BASE_SIZE] >> (i % BASE_SIZE)) & 1;
    }