#include "BigNumber.h"

BigNumber barrett(BigNumber &x, BigNumber &m, int m_len) {
    int tmpi = (2*m_len*2+1)/BASE_SIZE;
    if((2*m_len*2+1)%BASE_SIZE) tmpi++;
    BigNumber z(tmpi);

    if(x == z) return x; // Zero case

    z.digits[tmpi-1] = 1 << (2*m_len*2) % BASE_SIZE;
    z.len = z.get_maxLen();
    if(m.len > 1) {
        z = z / m;
    } else {
        BASE mb = m.digits[0];
        z = z / mb;
    }

    tmpi = ((m_len-1)*2 + 1)/BASE_SIZE;
    if(((m_len-1)*2 + 1)%BASE_SIZE) tmpi++;
    BigNumber tmp(tmpi), q_;
    tmp.digits[tmpi-1] = 1 << ((m_len-1)*2)%BASE_SIZE;
    tmp.len = tmp.get_maxLen();

    if(tmp.len > 1) {
        q_ = x / tmp;
    } else {
        BASE tmpb = tmp.digits[0];
        q_ = x / tmpb;
    }
    q_ = q_ * z;

    tmpi = ((m_len+1)*2 + 1)/BASE_SIZE;
    if(((m_len+1)*2 + 1)%BASE_SIZE) tmpi++;
    tmp = BigNumber(tmpi);
    tmp.digits[tmpi-1] = 1 << ((m_len+1)*2)%BASE_SIZE;
    tmp.len = tmp.get_maxLen();
    if(tmp.len > 1) {
        q_ = q_ / tmp;
    } else {
        BASE tmpb = tmp.digits[0];
        q_ = q_ / tmpb;
    }

    BigNumber r1(tmp.get_len()), r2(tmp.get_len()), tmp2;
    tmp2 = q_ * m;
    if(tmp.len > 1) {
        r1 = x % tmp;
        r2 = tmp2 % tmp;
    } else {
        BASE tmpb = tmp.digits[0], tmpb2, tmpb3;
        tmpb2 = x % tmpb;
        r1.digits[0] = tmpb2;
        r1.len = r1.get_maxLen();
        tmpb3 = tmp2 % tmpb;
        r2.digits[0] = tmpb3;
        r2.len = r2.get_maxLen();
    }

    BigNumber r;

    if(r1 >= r2) {
        r = r1 - r2;
    } else {
        r = tmp + r1;
        r = r - r2;
    }

    while(r >= m) {
        r = r - m;
    }

    return r;
}