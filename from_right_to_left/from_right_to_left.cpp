#include "BigNumber.h"

BigNumber from_right_to_left(BigNumber x, BigNumber y, BigNumber m) {
    BigNumber z; // z = 0
    BigNumber q; // q = 0
    q = x;
    if(!y[0]) {
        BASE one = 1;
        z = z + one;
    } else {
        z = x;
    }
    int y_len = BASE_SIZE * y.get_len();
    for(;!(y[y_len-1]); y_len--);
    for(int i = 1; i < y_len; i++) {
        q = q*q;
        q = q % m;
        if(y[i]) {
            z = z*q;
            z = z % m;
        }
    }
    return z;
}

