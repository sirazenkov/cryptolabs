#include "BigNumber.h"
#include <iostream>
#include <fstream>

void F(BigNumber *arr, BigNumber & a, BigNumber & g, BigNumber & n) {
    BASE one = 1, two = 2, three = 3; BigNumber bone; bone = bone + one;
    BigNumber p; p = n - bone;
    if(arr[0] % three == one) {
        arr[0] = a * arr[0];
        arr[0] = arr[0] % n;

        arr[1] = arr[1] + one;
        arr[1] = arr[1] % p;
    } else if(arr[0] % three == two) {
        arr[0] = arr[0] * arr[0];
        arr[0] = arr[0] % n;

        arr[1] = arr[1] * two;
        arr[1] = arr[1] % p;

        arr[2] = arr[2] * two;
        arr[2] = arr[2] % p;
    } else {
        arr[0] = g * arr[0];
        arr[0] = arr[0] % n;

        arr[2] = arr[2] + one;
        arr[2] = arr[2] % p;
    }
}

int main() {
    try {
        ifstream input("input.txt");
        ofstream output("output.txt");
        if(!(input.is_open() && output.is_open())) {
            throw("File does not exist!");
        }
        while(!input.eof()) {
            BigNumber n, a;
            char ns[100], as[100], ds[100];
            bool flag = true;
            char c;

            int i = 0;
            do {
                c = input.get();
                if ((c < '0' || c > '9') && c != ' ') {
                    flag = false;
                    while(c != '\n' && !input.eof()) {
                        c = input.get();
                    }
                } else if(c != ' ') {
                    ns[i++] = c;
                }
            } while (flag && c != ' ');
            if(!flag) continue;
            ns[i] = '\0';

            i = 0;
            do {
                c = input.get();
                if ((c < '0' || c > '9') && c != '\n') {
                    flag = false;
                    while(c != '\n' && !input.eof()) {
                        c = input.get();
                    }
                } else if(c != '\n') {
                    as[i++] = c;
                }
            } while (flag && c != '\n');
            if(!flag) continue;
            as[i] = '\0';

            n.base_10_input(ns);
            a.base_10_input(as);

            if(!n.isPrime() || a > n) {
                continue;
            }
                // Find generator
            //------------------------------------------
            BigNumber bone; bone = bone + (BASE)1;
            BigNumber g, n_copy; n_copy = n - bone;
            int k = int(extracting_the_root(n_copy, (BASE)2));
            BigNumber *primes; primes = new BigNumber[k];
            BigNumber zero, one; one = one + (BASE)1;
            // Generate k prime numbers
            //-------------------------------
            BigNumber bnum; bnum = bnum + (BASE)2;
            primes[0] = bnum;
            for(int i = 1; i < k;) {
                bnum = bnum + (BASE)1;
                flag = false;
                for(int j = 0; j < i; j++) {
                    if(bnum % primes[j] == zero) {
                        flag = true;
                        break;
                    }
                }
                if(flag) {
                    continue;
                }
                primes[i++] = bnum;
            }
            //-------------------------------
            vector<BigNumber> divisors;
            for(int i = 0; i < k; i++) {
                if(n_copy % primes[i] == zero) {
                    divisors.push_back(primes[i]);
                    while(n_copy % primes[i] == zero) {
                        n_copy = n_copy / primes[i];
                    }
                }
            }
            g = g + (BASE)2;
            for(; g < n-bone; g = g + (BASE)1) {
                bool is_generator = true;
                for(int j = 0; j < divisors.size(); j++) {
                    if(from_right_to_left(g,(n-bone)/divisors[j],n) == one) {
                        is_generator = false;
                        break;
                    }
                }
                if(is_generator) {
                    break;
                }
            }
            //------------------------------------------
            BigNumber arr1[3], arr2[3];
            arr1[0] = one, arr2[0] = one, arr1[1] = zero, arr1[2] = zero, arr2[1] = zero, arr2[2] = zero;
            do {
                F(arr1, a, g, n);
                F(arr2, a, g, n);
                F(arr2, a, g, n);
            } while(arr1[0] != arr2[0]);
            if(arr1[1] == arr2[1]) {
                continue;
            } else {
                BigNumber d, p, tmp, yy, zz; p = n - one;
                yy = arr2[1]- arr1[1], zz = arr1[2]- arr2[2];
                while(!yy.is_non_negative()) {
                    yy = yy + p;
                }
                while(!zz.is_non_negative()) {
                    zz = zz + p;
                }
                d = EEA(yy, p, &tmp);
                k = int(d);
                BigNumber *solutions = new BigNumber[k], solution; solution = one;
                for(int i = 0; i < k;) {
                    tmp = yy * solution;
                    tmp = tmp % p;
                    if(tmp == zz) {
                        solutions[i++] = solution;
                    }
                    solution = solution + (BASE)1;
                }
                for(int i = 0; i < k; i++) {
                    if(from_right_to_left(g, solutions[i], n) == a) {
                        d = solutions[i];
                        d.base_10_output(ds);
                        output << ds << (char)(0xa);
                        break;
                    }
                }
                delete [] primes;
                delete [] solutions;
            }
        }
        input.close();
        output.close();
    } catch(const char *msg) {
        cout << "Error: " << msg << '\n';
        return 0;
    } catch(...) {
        cout << "Unknown error!\n";
        return 0;
    }
    return 1;
}