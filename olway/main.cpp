#include "BigNumber.h"
#include <iostream>
#include <fstream>

int main() {
    try {
        ifstream input("input.txt");
        ofstream output("output.txt");
        if(!(input.is_open() && output.is_open())) {
            throw("File does not exist!");
        }
        while(!input.eof()) {
            BigNumber n, B;
            char ns[100], ds[100];
            bool flag = true;
            char c;

            int i = 0;
            do {
                c = input.get();
                if ((c < '0' || c > '9') && c != '\n') {
                    flag = false;
                    while(c != '\n' && !input.eof()) {
                        c = input.get();
                    }
                } else if(c != '\n') {
                    ns[i++] = c;
                }
            } while (flag && c != '\n');
            if(!flag) continue;
            ns[i] = '\0';

            n.base_10_input(ns);

            BASE one = 1, two = 2, three = 3;
            BigNumber btwo; btwo = btwo + two;
            BigNumber bone; bone = bone + one;

            while(!n.lsb() ) {
                n = n / btwo;
            }

            BigNumber d; d = extracting_the_root(n, three) * btwo + bone;
            BigNumber r1, r2, tmp; r1 = n % d; tmp = d - btwo; r2 = n % tmp;
            BigNumber q, s, zero; q = n/tmp; tmp = n / d; q = q - tmp; q = q * btwo * btwo;
            s = extracting_the_root(n, two);

            bool cont = false;
            while(true) {
                d = d + two;
                if(d > s) {
                    cont = true;
                    break;
                }
                BigNumber r; r = ((r1*btwo)-r2)+q;
                if(r < zero) {
                    r = r + d; q = q + two + two;
                }
                while(r >= d) {
                    r = r - d; q = q - btwo - btwo;
                }
                if(r == zero) {
                    break;
                } else {
                    r2 = r1; r1 = r;
                }
            }
            if(cont) {
                continue;
            }
            d.base_10_output(ds);
            output << ds << char(0xa);
        }
        input.close();
        output.close();
    } catch(const char *msg) {
        cout << "Error: " << msg << '\n';
        return 0;
    } catch(...) {
        cout << "Unknown error!\n";
        return 0;
    }
    return 1;
}
